package main

import "testing"

func TestGetConfigPath(t *testing.T) {
	if cfgPath, err := GetConfigPath(); err != nil {
		t.Error("did not figure out a config", nil)
	} else {
		t.Logf("cfgPath generated %s", cfgPath)
	}
}
