image:
  name: gcr.io/kaniko-project/executor:debug
  entrypoint: [""]

variables:
  DOCKER_BUILDKIT: 1
  PACKAGE_VERSION: "1.0.0"
  LINUX_AMD64_BINARY: "candies-linux-amd64-${PACKAGE_VERSION}"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/candies/${PACKAGE_VERSION}"

stages:
  - base
  - test
  - audit
  - build
  - upload
  - release

base_image:
  stage: base
  only:
    - develop
  script:
    - /kaniko/executor --no-push --skip-unused-stages=true --target base --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination ${CI_PROJECT_NAME}:${CI_COMMIT_SHORT_SHA}-base

test_image:
  stage: test
  only:
    - develop
  script:
    - /kaniko/executor --no-push --skip-unused-stages=true --target test --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination ${CI_PROJECT_NAME}:${CI_COMMIT_SHORT_SHA}-test
  allow_failure: true

audit_image:
  stage: audit
  only:
    - develop
  script:
    - /kaniko/executor --no-push --skip-unused-stages=true --target nancy --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination ${CI_PROJECT_NAME}:${CI_COMMIT_SHORT_SHA}-nancy

build_artifact:
  image: golang:1.16.0-alpine3.13
  variables:
    CGO_ENABLED: 0
    GOARCH: amd64
    GOOS: linux
  stage: build
  script:
    - env
    - mkdir bin/
    - go build -a -ldflags '-s -w -extldflags "-static"' -o bin/${LINUX_AMD64_BINARY} main.go
    - ls bin
  artifacts:
    paths:
      - bin/
    expire_in: 59 mins

upload:
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/${LINUX_AMD64_BINARY} ${PACKAGE_REGISTRY_URL}/${LINUX_AMD64_BINARY}

release:
  # Caution, as of 2021-02-02 these assets links require a login, see:
  # https://gitlab.com/gitlab-org/gitlab/-/issues/299384
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
        --assets-link "{\"name\":\"${LINUX_AMD64_BINARY}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${LINUX_AMD64_BINARY}\"}"

