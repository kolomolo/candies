package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/go-resty/resty/v2"
	"gopkg.in/yaml.v2"
	"log"
	"os"
	"strings"
	"time"
)

var TOKEN = os.Getenv("TOKEN")
var client = resty.New()

type Config struct {
	CandyJar struct {
		Endpoint         string        `yaml:"endpoint"`
		TargetDir        string        `yaml:"bag_path"`
		FilenameTemplate string        `yaml:"template"`
		Throttle         time.Duration `yaml:"throttle"`
		UserId           string        `yaml:"userid"`
	} `yaml:"candyjar"`
}

type ResultadoObj struct {
	IdDocumento     int    `json:"id_documento"`
	Nombre          string `json:"nombre"`
	Estado          string `json:"estado"`
	UsuarioCreacion int    `"_usuario_creacion"`
}

type DatosStruct struct {
	Total     int            `json:"total"`
	Resultado []ResultadoObj `json:"resultado"`
}

type DocList struct {
	TipoMensaje string      `json:"tipoMensaje"`
	Mensaje     string      `json:"mensaje"`
	Datos       DatosStruct `json:"datos"`
}

func (config Config) RetrieveMyDocs(from int) error {
	mylist, err := config.GetMyDocList()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(len(mylist))
	config.SaveList(mylist, from)
	return nil
}

func (config Config) RetrieveAllDocs(from int) error {
	mylist, err := config.GetAllDocsList()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(len(mylist))
	config.SaveList(mylist, from)
	return nil
}

func (config Config) GetMyDocList() ([]string, error) {
	requestUrl := config.CandyJar.Endpoint + "/api/v1/plantillasFormly/documento/" + config.CandyJar.UserId + "/misdocumentos/?order=-_fecha_modificacion&limit=1000&page=1&filter=&fields=id_documento,nombre,estado"
	log.Printf("Getting %s: ", requestUrl)

	var itemList []string

	resp, err := client.R().
		SetHeader("Authorization", "Bearer "+TOKEN).
		EnableTrace().
		Get(requestUrl)
	if err != nil {
		log.Fatal(err)
	}
	myResponse := DocList{}
	errz := json.Unmarshal(resp.Body(), &myResponse)
	if errz != nil {
		log.Fatal(errz)
	}
	for key, element := range myResponse.Datos.Resultado {
		log.Println("Key:", key, "=>", "Element:", element.Nombre)
		newFileNameFirst := strings.Replace(element.Nombre, "/", "", -1)
		newFileName := strings.Replace(newFileNameFirst, "-", "", -1)
		log.Println(newFileName)
		itemList = append(itemList, newFileName)
	}
	return itemList, nil
}

func (config Config) SaveList(doclist []string, from int) error {
	for index, each := range doclist {
		fmt.Printf("Candy value [%d] is [%s]\n", index, each)
		if index > from {
			SaveFile(config.CandyJar.Endpoint, each+".pdf")
			time.Sleep(config.CandyJar.Throttle)
		} else {
			log.Printf("skipping index: %d", index)
		}
	}
	return nil
}

func (config Config) GetAllDocsList() ([]string, error) {
	requestUrl := config.CandyJar.Endpoint + "/api/v1/plantillasFormly/documento?fields=id_documento,nombre,fecha&estado=APROBADO,CERRADO,DERIVADO&page=1&order=-fecha&limit=200000&searchPro=1&tipoBus=campo&campoSel=nombre&filter=A"
	log.Printf("Getting %s: ", requestUrl)

	var itemList []string

	resp, err := client.R().
		SetHeader("Authorization", "Bearer "+TOKEN).
		EnableTrace().
		Get(requestUrl)
	if err != nil {
		log.Fatal(err)
	}
	myResponse := DocList{}
	errz := json.Unmarshal(resp.Body(), &myResponse)
	if errz != nil {
		log.Fatal(errz)
	}
	for key, element := range myResponse.Datos.Resultado {
		log.Println("Key:", key, "=>", "Element:", element.Nombre)
		newFileNameFirst := strings.Replace(element.Nombre, "/", "", -1)
		newFileName := strings.Replace(newFileNameFirst, "-", "", -1)
		log.Println(newFileName)
		itemList = append(itemList, newFileName)
	}
	return itemList, nil
}

func (config Config) GetCode() error {
	codeUrl := config.CandyJar.Endpoint + "/codigo/"
	log.Printf("Getting %s: ", codeUrl)
	resp, err := client.R().
		EnableTrace().
		Get(codeUrl)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(resp)
	return nil
}

func SaveFile(endpoint string, fileName string) error {
	log.Printf("Saving filename %s from endpoint %s", fileName, endpoint)
	documentUrl := endpoint + "/api/v1/plantillasFormly/documentoPDF/"
	log.Println(documentUrl)
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+TOKEN).
		SetOutput(fileName).
		SetBody(map[string]interface{}{"cite": fileName}).
		Post(documentUrl)

	if err != nil {
		log.Fatal(err)
	}

	log.Println(resp)
	return nil
}

func GetCandyName(candyid int, template string) (string, error) {
	/* the number of zeros is different for each service */
	var zeroid = fmt.Sprintf("%04d", candyid)
	var fileName = strings.Replace(template, "#doc_id#", zeroid, -1)
	if fileName == "" {
		err := errors.New("filename got wrong")
		return "", err
	}
	return fileName, nil
}

func NewConfig(configPath string) (*Config, error) {
	config := &Config{}

	file, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	d := yaml.NewDecoder(file)

	if err := d.Decode(&config); err != nil {
		return nil, err
	}

	return config, nil
}

func ValidateConfigPath(path string) error {
	s, err := os.Stat(path)
	if err != nil {
		return err
	}
	if s.IsDir() {
		return fmt.Errorf("'%s' is a directory, not a normal file", path)
	}
	return nil
}

func GetConfigPath() (string, error) {

	configPath := os.Getenv("CONFIG_PATH")

	if len(configPath) == 0 {
		configPath = "config.yaml"
	}

	if err := ValidateConfigPath(configPath); err != nil {
		return "", err
	}
	return configPath, nil
}

func (config Config) Run() {

	log.Printf("token: %s", TOKEN)
	log.Println(config.CandyJar)

	client.SetOutputDirectory(config.CandyJar.TargetDir)

	log.Print("we did it")

}

func main() {

	if TOKEN == "" {
		log.Fatal("No token found. You must provide a token with the environment variable TOKEN")
	}

	cfgPath, err := GetConfigPath()

	if err != nil {
		log.Fatal(err)
	}
	cfg, err := NewConfig(cfgPath)
	if err != nil {
		log.Fatal(err)
	}

	client.SetOutputDirectory(cfg.CandyJar.TargetDir)

	action := flag.String("action", "", "The action to perform. Available actions: code, mydoclist, alldocs")
	from := flag.Int("from", 0, "A number representing the element from which to resume a download")
	flag.Parse()
	log.Printf("action: %s", *action)
	log.Printf("from: %d", *from)
	switch a := *action; a {
	case "code":
		cfg.GetCode()
	case "mydoclist":
		f := *from
		cfg.RetrieveMyDocs(f)
	case "alldocs":
		f := *from
		cfg.RetrieveAllDocs(f)
	default:
		flag.PrintDefaults()
	}
}
