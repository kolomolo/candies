module main.go

go 1.15

require (
	github.com/go-resty/resty/v2 v2.5.0
	gopkg.in/yaml.v2 v2.3.0
)
