FROM golang:1.16.0-alpine3.13 as base
RUN apk add --no-cache \
    git \
  && adduser -u 1000 -D candies \
  && mkdir /app \
  && chown -R candies:candies /app
WORKDIR /app
USER candies
COPY --chown=candies:candies ./go.mod ./go.sum ./
RUN go mod download

# stage dev
FROM base as dev
RUN go get github.com/cespare/reflex
CMD ["reflex", "-d", "none", "-c", "reflex.conf"]

# stage source
FROM base as source
COPY --chown=candies:candies . .
RUN mv config.yaml.sample config.yaml

# stage build
FROM source as build
ENV CGO_ENABLED=0
ENV GOARCH=amd64
ENV GOOS=linux
RUN go build \
  -a -ldflags '-s -w -extldflags "-static"' \
  main.go

# stage test
FROM source as test 
ENV CGO_ENABLED=0
ENV GOARCH=amd64
ENV GOOS=linux
RUN go test
CMD ["go", "test"]

# stage nancy
FROM source as nancy
ARG NANCY_VERSION=v1.0.0
RUN wget -O ./nancy \
    https://github.com/sonatype-nexus-community/nancy/releases/download/${NANCY_VERSION}/nancy-linux.amd64-${NANCY_VERSION} \
  && chmod +x ./nancy
CMD go list -json -m all | ./nancy sleuth

# stage runtime
FROM scratch as runtime
LABEL maintainer="Kolo Molo thisiskolomolo@yandex.ru"
LABEL org.label-schema.description="candies"
LABEL org.label-schema.name="candies"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.vendor="kolomolo"
ENV CONFIG_PATH=/config.yaml
COPY --from=build /app/config.yaml /config.yaml
COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /app/main /usr/bin/candies
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
USER candies
CMD ["candies"]
