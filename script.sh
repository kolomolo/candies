#!/usr/bin/env sh

set -e

TARGET_DIR="$HOME/.mycandybag/"
mkdir -p $TARGET_DIR

THROTTLE=2
FILENAME_TEMPLATE="ENTIDY#doc_id#2021.pdf"
TOKEN=$1

echo "doing something funny..."

get_candy() {
  FILENAME="${FILENAME_TEMPLATE/\#doc\_id\#/$1}"
  echo "retrieving candy ... ${FILENAME}"
  CITE_JSON='{"cite":"'$FILENAME'"}'
  curl 'https://endpoint.org/plantillas/ws/api/v1/plantillasFormly/documentoPDF/' \
    --compressed \
    -H "Content-Type: application/json;charset=utf-8" \
    -H "Authorization: Bearer $TOKEN" \
    --data-raw $CITE_JSON \
    -o $TARGET_DIR$FILENAME
  sleep $THROTTLE
}

for i in {0001..0099}; do
  get_candy $i
done
